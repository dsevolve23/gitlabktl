package knative

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestApplicationDeploy(t *testing.T) {
	service := Service{
		Name:      "my-service",
		Namespace: "my-namespace",
		Image:     "registry.gitlab.com/my/source",
		Secrets:   []string{"my-secret"},
		Envs:      []string{"my-env=my-env-value"},
	}

	config := new(MockConfig)
	config.On("Registry").
		Return(Registry{Username: "user", Password: "password"}).
		Once()
	defer config.AssertExpectations(t)

	cluster := new(MockCluster)
	cluster.On("SetNoDryRun").Once()
	cluster.On("DeployRegistryCredentials").Return(nil).Once()
	cluster.On("DeployService", service).Return("deployed", nil).Once()
	defer cluster.AssertExpectations(t)

	app := &Application{service: service, cluster: cluster, config: config}

	summary, err := app.Deploy(context.Background())
	require.NoError(t, err)

	assert.Equal(t, "deployed", summary)
}

func TestApplicationDryRunDeploy(t *testing.T) {
	service := Service{
		Name:      "my-service",
		Namespace: "my-namespace",
		Image:     "registry.gitlab.com/my/source",
		Secrets:   []string{"my-secret"},
		Envs:      []string{"my-env=my-env-value"},
	}

	cluster := new(MockCluster)
	cluster.On("SetDryRun").Once()
	cluster.On("DeployService", service).Return("summary", nil).Once()
	defer cluster.AssertExpectations(t)

	app := &Application{service: service, cluster: cluster}

	summary, err := app.DeployDryRun()
	require.NoError(t, err)

	assert.Equal(t, "summary", summary)
}
