package image

import (
	"net/url"
	"strings"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/gitlabktl/registry"
)

// This type represents a Docker image with credentials used to authenticate
// to a registry that stores it
type Image struct {
	Repository string   // image repository eg. `registry.gitlab.com/my/project`
	Tag        string   // image tag eg. `0.1.0`
	Aliases    []string // additional tags pointing to the same image eg. `latest, test`
	Username   string   // registry username
	Password   string   // registry password
}

// ToString() returns a full image name (with a tag)
func (image Image) ToString() string {
	repository := image.repository()

	if len(repository) == 0 {
		panic("unable to determine image destination repository")
	}

	if len(image.Tag) > 0 {
		return strings.Join([]string{repository, image.Tag}, ":")
	}

	return strings.Join([]string{repository, "latest"}, ":")
}

func (image Image) ToRepository() string {
	return image.repository()
}

// All locations of the image, including all aliases of the tag
func (image Image) Locations() []string {
	if len(image.Aliases) > 0 && len(image.Tag) == 0 {
		panic("image tag not provided but aliases present")
	}

	repository := image.repository()
	tags := append([]string{image.Tag}, image.Aliases...)
	locations := make([]string, len(tags))

	for i, tag := range tags {
		locations[i] = strings.Join([]string{repository, tag}, ":")
	}

	return locations
}

// PushRegistry() returns details of a registry that one can use to push this
// image to
func (image Image) PushRegistry() registry.Registry {
	if image.useDefaultRegistry() {
		return registry.NewWithPushAccess()
	}

	return image.customRegistry()
}

// PushRegistry() returns details of a registry that one can use to pull this
// image from. This is also valid after GitLab CI/CD job is complete.
func (image Image) PullRegistry() registry.Registry {
	if image.useDefaultRegistry() {
		return registry.NewWithPullAccess()
	}

	return image.customRegistry()
}

func (image Image) repository() string {
	if len(image.Repository) > 0 {
		return image.Repository
	}

	return registry.DefaultRepository()
}

func (image Image) customRegistry() registry.Registry {
	return registry.Registry{
		Host:        image.registryHost(),
		Credentials: image.registryCredentials(),
	}
}

func (image Image) useDefaultRegistry() bool {
	if image.usingCustomCredentials() {
		return false
	}

	return registry.IsDefaultRegistry(image.repository())
}

func (image Image) usingCustomCredentials() bool {
	return len(image.Username)+len(image.Password) > 0
}

func (image Image) registryCredentials() registry.Credentials {
	return registry.Credentials{
		Username: image.Username,
		Password: image.Password,
	}
}

func (image Image) registryHost() string {
	address, err := url.Parse("http://" + image.repository())

	if err != nil {
		logrus.WithError(err).Fatal("could not parse registry image repository location")
	} else {
		logrus.WithField("registry", address.Host).Info("using registry")
	}

	if len(address.Host) == 0 {
		logrus.Fatal("could not extract registry location")
	}

	return address.Host
}
