package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func TestKubeConfigProvided(t *testing.T) {
	fs.WithTestFs(func() {
		config := KubeConfig{Path: "/my/.kubeconfig"}
		err := fs.WriteFile("/my/.kubeconfig", []byte("k8s"), 0644)
		require.NoError(t, err)

		assert.False(t, config.IsMissing())
		assert.Equal(t, "/my/.kubeconfig", config.FilePath())
	})
}

func TestKubeConfigNotProvided(t *testing.T) {
	fs.WithTestFs(func() {
		config := KubeConfig{}
		err := fs.WriteFile("/.kubeconfig", []byte("k8s"), 0644)
		require.NoError(t, err)

		env.WithStubbedEnv(env.Stubs{"KUBECONFIG": "/.kubeconfig"}, func() {
			assert.False(t, config.IsMissing())
			assert.Equal(t, "/.kubeconfig", config.FilePath())
		})
	})
}

func TestKubeConfigMissing(t *testing.T) {
	config := KubeConfig{}

	env.WithStubbedEnv(env.Stubs{"KUBECONFIG": "/missing/.kubeconfig"}, func() {
		assert.True(t, config.IsMissing())
	})
}
