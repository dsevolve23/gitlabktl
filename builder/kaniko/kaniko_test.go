package kaniko

import (
	"context"
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

func newKaniko() Kaniko {
	return Kaniko{
		Dockerfile:   "Dockerfile.tmp",
		Workspace:    "build/",
		Destinations: []string{"my-image:1", "my-image:2"},
		Registry: registry.Registry{
			Host: "registry",
			Credentials: registry.Credentials{
				Username: "my-user",
				Password: "my-pass",
			},
		},
	}
}

func TestBuildWithMissingDockerfile(t *testing.T) {
	t.Run("with missing Dockerfile", func(t *testing.T) {
		fs.WithTestFs(func() {
			kaniko := newKaniko()

			err := kaniko.Build(context.Background())

			assert.Equal(t, "dockerfile not found", err.Error())
		})
	})

	t.Run("with generated authfile", func(t *testing.T) {
		fs.WithTestFs(func() {
			kaniko := newKaniko()

			err := fs.WriteFile(kaniko.Dockerfile, []byte("FROM scratch"), 0644)
			require.NoError(t, err)

			err = kaniko.Build(context.Background())

			auth, err := fs.ReadFile(authfile)
			require.NoError(t, err)

			assert.Contains(t, string(auth), "my-user")
		})
	})

	t.Run("with kaniko executor binary missing", func(t *testing.T) {
		fs.WithTestFs(func() {
			kaniko := newKaniko()

			err := fs.WriteFile(kaniko.Dockerfile, []byte("FROM scratch"), 0644)
			require.NoError(t, err)

			err = kaniko.Build(context.Background())

			assert.Contains(t, err.Error(), "/kaniko/executor: no such file or directory")
		})
	})

	t.Run("with Dockerfile and nested builder", func(t *testing.T) {
		before := new(MockBuilder)
		before.On("Build").Return(nil).Once()
		defer before.AssertExpectations(t)

		fs.WithTestFs(func() {
			kaniko := newKaniko()
			kaniko.Before = []Builder{before}

			err := fs.WriteFile(kaniko.Dockerfile, []byte("FROM scratch"), 0644)
			require.NoError(t, err)

			err = kaniko.Build(context.Background())

			assert.Contains(t, err.Error(), "/kaniko/executo")
		})
	})

	t.Run("when nested builder returns an error", func(t *testing.T) {
		nestedBuilderError := errors.New("nested-builder-error")
		before := new(MockBuilder)
		before.On("Build").Return(nestedBuilderError).Once()

		fs.WithTestFs(func() {
			kaniko := newKaniko()
			kaniko.Before = []Builder{before}

			err := kaniko.Build(context.Background())

			assert.Equal(t, err.Error(), "nested-builder-error")
		})
	})
}

func TestBuildDryRun(t *testing.T) {
	t.Run("without nested builders", func(t *testing.T) {
		fs.WithTestFs(func() {
			kaniko := newKaniko()

			err := fs.WriteFile(kaniko.Dockerfile, []byte("FROM scratch"), 0644)
			require.NoError(t, err)
			summary, err := kaniko.BuildDryRun()
			assert.NoError(t, err)

			assert.Contains(t, summary, "/kaniko/executor")
			assert.Contains(t, summary, "FROM scratch")
		})
	})

	t.Run("with nested builder", func(t *testing.T) {
		builder := new(MockBuilder)
		builder.On("BuildDryRun").Return("dry-run-summary", nil).Once()

		fs.WithTestFs(func() {
			kaniko := newKaniko()
			kaniko.Before = []Builder{builder}

			summary, err := kaniko.BuildDryRun()
			require.NoError(t, err)

			assert.Contains(t, summary, "dry-run-summary")
		})
	})

	t.Run("with nested builder returning an error", func(t *testing.T) {
		builder := new(MockBuilder)
		builder.On("BuildDryRun").Return("dry-run-summary", errors.New("nested error")).Once()

		fs.WithTestFs(func() {
			kaniko := newKaniko()
			kaniko.Before = []Builder{builder}

			summary, err := kaniko.BuildDryRun()

			assert.Equal(t, "nested error", err.Error())
			assert.Contains(t, summary, "nested kaniko before builder error")
		})
	})
}

func TestExecutorCommand(t *testing.T) {
	kaniko := newKaniko()

	command := kaniko.commandToString()

	assert.Equal(t, command,
		strings.Join([]string{"/kaniko/executor",
			"--context build/",
			"--dockerfile Dockerfile.tmp",
			"--destination my-image:1",
			"--destination my-image:2",
			"--cleanup"}, " "),
	)
}

func TestNewWriteAuthFile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		kaniko.writeAuthFile()
		output, err := fs.ReadFile(authfile)
		require.NoError(t, err)

		assert.Contains(t, string(output), "auths")
	})
}

func TestExistingWriteAuthFile(t *testing.T) {
	fs.WithTestFs(func() {
		kaniko := newKaniko()
		err := fs.WriteFile(authfile, []byte("stub"), 0644)
		require.NoError(t, err)

		kaniko.writeAuthFile()
		output, err := fs.ReadFile(authfile)
		require.NoError(t, err)

		assert.Contains(t, string(output), "auths")
	})
}
