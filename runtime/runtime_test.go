package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	details := Details{RuntimeAddress: "example.com/runtime"}
	assert.IsType(t, GeneratedRuntime{}, New(details))

	details = Details{FunctionName: "my-function"}
	assert.IsType(t, CustomRuntime{}, New(details))
}
