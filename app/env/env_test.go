package env

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOsEnv(t *testing.T) {
	_, ok := New().LookupEnv("HOME")
	assert.True(t, ok)

	_, ok = New().LookupEnv("MISSING")
	assert.False(t, ok)
}

func TestGetenv(t *testing.T) {
	WithStubbedEnv(Stubs{"NEW_ENV": "new env"}, func() {
		assert.Equal(t, "new env", Getenv("NEW_ENV"))
	})
}

func TestStubbedEnv(t *testing.T) {
	WithStubbedEnv(Stubs{"HOME": "/my/home", "USER": "me"}, func() {
		myenv := New()

		assert.Equal(t, "me", myenv.Getenv("USER"))
		assert.Equal(t, "/my/home", myenv.Getenv("HOME"))

		_, ok := myenv.LookupEnv("HOME")
		assert.True(t, ok)

		_, ok = myenv.LookupEnv("MISSING")
		assert.False(t, ok)
	})
}

func TestStubbedEnvScope(t *testing.T) {
	assert.Empty(t, New().Getenv("NEW_ENV"))

	WithStubbedEnv(Stubs{"NEW_ENV": "new env"}, func() {
		assert.Equal(t, "new env", New().Getenv("NEW_ENV"))
	})

	assert.Empty(t, New().Getenv("NEW_ENV"))
}

func TestDefined(t *testing.T) {
	WithStubbedEnv(Stubs{"NEW_ENV": "new env"}, func() {
		assert.True(t, Defined("NEW_ENV"))
	})

	WithStubbedEnv(Stubs{"NEW_ENV": "new env"}, func() {
		assert.False(t, Defined("NEW_ENV", "MISSING"))
	})
}
