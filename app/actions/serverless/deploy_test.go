package serverless

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/triggermesh/tm/pkg/resources/service"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/knative"
)

func TestServerlessDryRunDeployment(t *testing.T) {
	buffer := new(bytes.Buffer)
	service.Output = buffer
	defer func() { service.Output = os.Stdout }()

	action := DeployAction{
		DryRun:         true,
		ServerlessFile: "../../../config/testdata/serverless.yml",
		KubeConfigFile: "../../../config/testdata/kubeconfig.json",
	}

	envs := env.Stubs{
		"CI_REGISTRY":           "my.registry",
		"CI_PROJECT_VISIBILITY": "public",
	}

	env.WithStubbedEnv(envs, func() {
		err := action.Execute(new(commands.Context))
		require.NoError(t, err)
	})

	output := buffer.String()
	assert.Contains(t, output, `"kind": "Service"`)
	assert.Contains(t, output, `"apiVersion": "serving.knative.dev/v1alpha1"`)
	assert.Contains(t, output, `"image": "registry.gitlab.com/my-project/my-functions-echo"`)
	assert.Contains(t, output, `"namespace": "testNamespace"`)
	assert.Contains(t, output, `"name": "my-k8s-secret"`)
}

func TestServerlessDeployment(t *testing.T) {
	action := DeployAction{
		ServerlessFile: "../../../config/testdata/serverless.yml",
		KubeConfigFile: "../../../config/testdata/kubeconfig.json",
	}

	t.Run("when project is public", func(t *testing.T) {
		envs := env.Stubs{
			"CI_REGISTRY":           "my.registry",
			"CI_PROJECT_VISIBILITY": "public",
		}

		knative.WithMockCluster(func(cluster *knative.MockCluster) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("DeployFunctions", mock.Anything, mock.Anything).
				Return("deployed", nil).Once()
			defer cluster.AssertExpectations(t)

			env.WithStubbedEnv(envs, func() {
				assert.NoError(t, action.Execute(new(commands.Context)))
			})
		})
	})

	t.Run("when project is private", func(t *testing.T) {
		envs := env.Stubs{
			"CI_PROJECT_VISIBILITY": "private",
			"CI_REGISTRY":           "my.registry",
			"CI_DEPLOY_USER":        "user",
			"CI_DEPLOY_PASSWORD":    "pass",
		}

		knative.WithMockCluster(func(cluster *knative.MockCluster) {
			cluster.On("SetNoDryRun").Once()
			cluster.On("DeployRegistryCredentials").Return(nil).Once()
			cluster.On("DeployFunctions", mock.Anything, mock.Anything).
				Return("deployed", nil).Once()
			defer cluster.AssertExpectations(t)

			env.WithStubbedEnv(envs, func() {
				assert.NoError(t, action.Execute(new(commands.Context)))
			})
		})
	})
}
