package knative

import (
	"context"

	"github.com/sirupsen/logrus"
)

// Application representa a Knative application that can be deployed to a
// cluster.
type Application struct {
	config  Config
	cluster Cluster
	service Service
}

func (app *Application) DeployDryRun() (string, error) {
	logrus.Info("running application dry-run deployment")

	app.cluster.SetDryRun()

	return app.cluster.DeployService(app.service)
}

func (app *Application) Deploy(ctx context.Context) (string, error) {
	logrus.Info("running application deployment")

	app.cluster.SetNoDryRun()

	if app.config.Registry().HasCredentials() {
		app.cluster.DeployRegistryCredentials()
	}

	return app.cluster.DeployService(app.service)
}
