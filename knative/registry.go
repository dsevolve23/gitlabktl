package knative

// Registry represents Knative entry describing a container registry
type Registry struct {
	Host       string // Repository hostname
	Username   string // Access username
	Password   string // Access password
	Repository string // Default repository (includes the hostname)
}

func (registry Registry) HasCredentials() bool {
	return len(registry.Username) > 0 && len(registry.Password) > 0
}
