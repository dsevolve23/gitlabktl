package knative

import (
	"github.com/stretchr/testify/mock"
)

type MockCluster struct {
	mock.Mock
}

func (cluster *MockCluster) SetDryRun() {
	cluster.Called()
}

func (cluster *MockCluster) SetNoDryRun() {
	cluster.Called()
}

func (cluster *MockCluster) Namespace() string {
	args := cluster.Called()

	return args.String(0)
}

func (cluster *MockCluster) DeployRegistryCredentials() error {
	args := cluster.Called()

	return args.Error(0)
}

func (cluster *MockCluster) DeployService(service Service) (string, error) {
	args := cluster.Called(service)

	return args.String(0), args.Error(1)
}

func (cluster *MockCluster) DeployFunctions(service Service, manifest string) (string, error) {
	args := cluster.Called(service, manifest)

	return args.String(0), args.Error(1)
}

func WithMockCluster(block func(mock *MockCluster)) {
	defer setDefaultClusterFactory()

	mock := new(MockCluster)

	clusterFactory = func(config Config) (Cluster, error) {
		return mock, nil
	}

	block(mock)
}
