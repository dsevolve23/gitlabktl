package runtime

import (
	"context"
	"path"

	"github.com/gosimple/slug"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const template = "Dockerfile.template"

type GeneratedRuntime struct {
	Details
}

// Build runtime files and persist them in CodeDirectory
func (runtime GeneratedRuntime) Build(ctx context.Context) error {
	if len(runtime.RuntimeAddress) == 0 {
		panic("invalid runtime fabricated")
	}

	logrus.WithField("runtime", runtime.DockerfilePath()).
		Info("preparing a generated runtime")

	repository := NewRepository(runtime.RuntimeAddress)
	dockerfile, err := repository.ReadFile(template)

	if err != nil {
		logrus.WithField("runtime", runtime.RuntimeAddress).
			WithError(err).Warn("could not read Dockerfile template")

		return errors.Wrap(err, "could not read Dockerfile template")
	}

	template := Template{
		Filename:   runtime.DockerfilePath(),
		Attributes: runtime.Details,
		Contents:   dockerfile,
	}

	err = template.Write()
	if err != nil {
		logrus.WithField("template", runtime.DockerfilePath()).
			WithError(err).Warn("could not write a runtime template")

		return errors.Wrap(err, "could not write a runtime template")
	}

	err = repository.WriteFiles(runtime.CodeDirectory)
	if err != nil {
		logrus.WithField("runtime", runtime.RuntimeAddress).
			WithError(err).Warn("could not write runtime context files")

		return errors.Wrap(err, "could not write runtime context files")
	}

	return nil
}

func (runtime GeneratedRuntime) BuildDryRun() (string, error) {
	if len(runtime.RuntimeAddress) == 0 {
		return "generated runtime error", errors.New("invalid runtime fabrication")
	}

	logrus.WithField("runtime", runtime.DockerfilePath()).
		Info("preparing a generated runtime (dry-run)")

	return "preparing a generated runtime", nil
}

func (runtime GeneratedRuntime) Slug() string {
	return slug.Make(runtime.FunctionName)
}

func (runtime GeneratedRuntime) DockerfilePath() string {
	filename := "Dockerfile." + runtime.Slug()

	return path.Join(runtime.CodeDirectory, filename)
}
