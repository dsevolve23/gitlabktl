// Package kaniko is responsible for performing a Kaniko build of an image.
// Kaniko builder does not require Docker Engine presence, therefore it is a
// better solution one can using within Continuous Integration context.
package kaniko

import (
	"bytes"
	"context"
	"errors"
	"os"
	"os/exec"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/gitlabktl/app/fs"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

// Kaniko represent a Kaniko build process.
type Kaniko struct {
	Before       []Builder
	Dockerfile   string            // Path to a Dockerfile
	Workspace    string            // build context / working directory
	Destinations []string          // destinations of an image
	Registry     registry.Registry // Docker registry
}

// Nested Kaniko builders interface
type Builder interface {
	BuildDryRun() (string, error)
	Build(ctx context.Context) error
}

const (
	executor = "/kaniko/executor"
	authfile = "/kaniko/.docker/config.json"
)

func (kaniko *Kaniko) Build(ctx context.Context) error {
	logrus.Info("running Kaniko before builders")

	for _, builder := range kaniko.Before {
		err := builder.Build(ctx)
		if err != nil {
			return err
		}
	}

	logrus.Info("building runtime image using Kaniko")

	err := kaniko.requireDockerfile()
	if err != nil {
		return err
	}

	err = kaniko.writeAuthFile()
	if err != nil {
		return err
	}

	return kaniko.execute(ctx)
}

func (kaniko *Kaniko) BuildDryRun() (string, error) {
	var summary bytes.Buffer

	for _, builder := range kaniko.Before {
		details, err := builder.BuildDryRun()
		if err != nil {
			return "nested kaniko before builder error", err
		}

		summary.WriteString("before build: " + details)
	}

	logrus.Info("building runtime image using Kaniko (dry-run!)")
	logrus.WithField("Dockerfile", kaniko.Dockerfile).Info("using Dockerfile")
	logrus.WithField("command", kaniko.commandToString()).Info("using command")

	// TODO in the future we might want to reuse a runtime BuildDryRun
	// to provide more information about building a runtime
	summary.WriteString("Dockerfile path: " + kaniko.Dockerfile + "\n")
	dockerfile, _ := fs.ReadFile(kaniko.Dockerfile)
	summary.WriteString("Dockerfile contents:\n" + string(dockerfile) + "\n")
	summary.WriteString(kaniko.commandToString())

	return summary.String(), nil
}

func (kaniko *Kaniko) requireDockerfile() error {
	if exists, _ := fs.Exists(kaniko.Dockerfile); !exists {
		logrus.WithField("dockerfile", kaniko.Dockerfile).Warn("dockerfile not found")
		return errors.New("dockerfile not found")
	}

	return nil
}

func (kaniko *Kaniko) execute(ctx context.Context) error {
	logrus.WithField("command", kaniko.commandToString()).Info("executing command")
	err := kaniko.executorCmd(ctx).Run()

	if err != nil {
		logrus.WithError(err).Warn("could not execute Kaniko build")
	}

	return err
}

func (kaniko *Kaniko) executorCmd(ctx context.Context) *exec.Cmd {
	args := []string{
		"--context", kaniko.Workspace,
		"--dockerfile", kaniko.Dockerfile,
	}

	for _, destination := range kaniko.Destinations {
		args = append(args, "--destination", destination)
	}

	command := exec.CommandContext(ctx, executor, append(args, "--cleanup")...)
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr

	return command
}

func (kaniko *Kaniko) commandToString() string {
	cmd := kaniko.executorCmd(context.Background())

	return strings.Join(cmd.Args, " ")
}

func (kaniko *Kaniko) writeAuthFile() error {
	if exists, _ := fs.Exists(authfile); exists {
		logrus.WithField("path", authfile).Warn("file already exists, overwriting")
	}

	file, err := fs.Create(authfile)
	if err != nil {
		return err
	}

	defer file.Close()

	_, err = file.Write([]byte(kaniko.authFileContents()))

	return err
}

func (kaniko *Kaniko) authFileContents() string {
	contents, err := kaniko.Registry.ToAuthFileContents()
	if err != nil {
		logrus.WithError(err).Fatal("could not generate docker auth file")
	}

	return contents
}
