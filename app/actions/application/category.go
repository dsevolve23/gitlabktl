package application

import (
	"gitlab.com/gitlab-org/gitlabktl/app/commands"
)

func newApplicationCategory() commands.Category {
	return commands.Category{
		Registrable: commands.Registrable{
			Path: "application",
			Config: commands.Config{
				Name:        "application",
				Usage:       "Manage your serverless applications",
				Aliases:     []string{"app"},
				Description: "Manage your serverless applications",
			},
		},
	}
}

func RegisterCategory(register *commands.Register) {
	register.RegisterCategory(newApplicationCategory())
}
