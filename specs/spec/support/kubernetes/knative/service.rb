module Kubernetes
  module Knative
    class Service
      def initialize(resource)
        raise ArgumentError unless resource.kind == 'Service'

        @service = resource
      end

      def name
        @service.dig(:metadata, :name)
      end

      def namespace
        @service.dig(:metadata, :namespace)
      end

      def domain
        @service.dig(:status, :domain)
      end

      def endpoint
        "http://#{domain}"
      end

      def env
        @service
          .dig(*%w[spec runLatest configuration revisionTemplate spec container env])
          .to_a.map { |resource| { name: resource.name, value: resource.value } }
      end
    end
  end
end
