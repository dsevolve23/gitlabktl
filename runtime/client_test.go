package runtime

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func newTestClient() (Client, error) {
	return NewClient(Location{
		Address:   "testdata/runtime",
		Reference: "master",
	})
}

func TestNewClient(t *testing.T) {
	_, err := newTestClient()

	assert.NoError(t, err)
}

func TestClientReadFile(t *testing.T) {
	client, err := newTestClient()
	require.NoError(t, err)

	function, err := client.ReadFile("function.rb")
	require.NoError(t, err)

	template, err := client.ReadFile("Dockerfile.template")
	require.NoError(t, err)

	assert.Contains(t, function, "self.invoke")
	assert.Contains(t, template, "FROM my-custom-invoker")
}

func TestClientWriteFiles(t *testing.T) {
	client, err := newTestClient()
	require.NoError(t, err)

	fs.WithTestFs(func() {
		err = client.WriteFiles("sub/directory")
		require.NoError(t, err)

		invoker, err := fs.ReadFile("sub/directory/function.rb")
		require.NoError(t, err)

		template, err := fs.ReadFile("sub/directory/Dockerfile.template")
		require.NoError(t, err)

		assert.Contains(t, string(invoker), "self.invoke")
		assert.Contains(t, string(template), "FROM my-custom-invoker")
	})
}
