package config

import (
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

func newTestServerlessManifest(t *testing.T) Serverless {
	fixture, err := ioutil.ReadFile("testdata/serverless.yml")
	require.NoError(t, err)

	return fs.WithTestFsValue(func() interface{} {
		err = fs.WriteFile(ServerlessFile, fixture, 0644)
		require.NoError(t, err)

		return NewServerlessConfig()
	}).(Serverless)
}

func TestNewServerlessConfig(t *testing.T) {
	config := newTestServerlessManifest(t)

	assert.Equal(t, "my-functions", config.Service)
}

func TestServerlessFunctions(t *testing.T) {
	functions := newTestServerlessManifest(t).ToFunctions()

	assert.Equal(t, 1, len(functions))
	assert.Equal(t, "echo", functions[0].Name)
	assert.Equal(t, "my-functions-echo", functions[0].Service)
	assert.Equal(t, "registry.gitlab.com/my-project/my-functions-echo",
		functions[0].Runtime.ResultingImage)
}
