package runtime

import (
	"github.com/sirupsen/logrus"
)

var (
	newRepositoryClient = func(location Location) (Client, error) {
		return NewClient(location)
	}
)

type Repository struct {
	Location
	Client
}

type Location struct {
	Address   string
	Reference string
}

func NewRepository(address string) *Repository {
	location := Location{Address: address, Reference: "master"}

	client, err := newRepositoryClient(location)
	if err != nil {
		logrus.WithError(err).Fatal("could not create a runtime repository client")
	}

	return &Repository{Location: location, Client: client}
}
