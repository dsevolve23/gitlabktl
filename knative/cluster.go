package knative

import (
	"path"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/triggermesh/tm/pkg/client"
	"github.com/triggermesh/tm/pkg/resources/credential"
)

func init() {
	setDefaultClusterFactory()
}

var clusterFactory func(config Config) (Cluster, error)

type Cluster interface {
	SetDryRun()
	SetNoDryRun()
	Namespace() string
	DeployRegistryCredentials() error
	DeployService(service Service) (string, error)
	DeployFunctions(service Service, manifest string) (string, error)
}

func NewCluster(config Config) (Cluster, error) {
	return clusterFactory(config)
}

func setDefaultClusterFactory() {
	clusterFactory = func(config Config) (Cluster, error) {
		return newTriggermeshCluster(config)
	}
}

// Triggermesh represent a Triggermesh cluster that we want to interface with
// using Triggermesh's `tm` tool.
type tmCluster struct {
	clientset *client.ConfigSet
	registry  Registry
}

// FIXME tm depends on a singleton / global state
func (cluster *tmCluster) SetDryRun() {
	logrus.Debug("setting a cluster dry-run mode")

	client.Dry = true
}

// FIXME tm depends on a singleton / global state
func (cluster *tmCluster) SetNoDryRun() {
	logrus.Debug("setting a real cluster interactions mode")

	client.Dry = false
}

// FIXME tm depends on a singleton / global state
func (cluster *tmCluster) Namespace() string {
	return client.Namespace
}

func (cluster *tmCluster) DeployRegistryCredentials() error {
	if !cluster.registry.HasCredentials() {
		return errors.New("missing registry credentials")
	}

	logrus.Info("deploying registry credentials")

	tmCreds := credential.RegistryCreds{
		Name:      "gitlab-registry",
		Namespace: client.Namespace,
		Host:      cluster.registry.Host,
		Username:  cluster.registry.Username,
		Password:  cluster.registry.Password,
		Pull:      true,
	}

	return tmCreds.CreateRegistryCreds(cluster.clientset)
}

func (cluster *tmCluster) DeployService(service Service) (string, error) {
	logrus.Info("deploying a service to a cluster")

	tmservice, err := newTriggermeshService(service, cluster)
	if err != nil {
		return "deployment failed", errors.Wrap(err, "could not define a service")
	}

	return tmservice.Deploy(cluster.clientset)
}

func (cluster *tmCluster) DeployFunctions(service Service, manifest string) (string, error) {
	deployer, err := newTriggermeshService(service, cluster)
	if err != nil {
		return "deployment error", errors.Wrap(err, "could not build deployment service")
	}

	funcs, err := deployer.ManifestToServices(manifest)
	if err != nil {
		return "deployment error", errors.Wrap(err, "could not parse serverless functions manifest")
	}

	// TODO This code belongs to Functions deployer, but it is here until we
	// refactor the way we handle serverless.yml manifest
	for index, function := range funcs {
		if len(deployer.Registry) > 0 {
			funcs[index].Source = path.Join(deployer.Registry, function.Name)
		} else {
			// TODO we should use cluster registry details herep probably
			funcs[index].Source = path.Join(cluster.registry.Repository, function.Name)
		}

		funcs[index].Buildtemplate = "" // We need to unset tm`s BuildTemplate
	}

	return "", deployer.DeployFunctions(funcs, true, 3, cluster.clientset)
}

func newTriggermeshCluster(config Config) (*tmCluster, error) {
	client.Namespace = config.Namespace()
	client.Wait = true

	clientset, err := client.NewClient(config.KubeConfig())
	if err != nil {
		errors.Wrap(err, "could not read cluster config")
	}

	return &tmCluster{clientset: &clientset, registry: config.Registry()}, nil
}
