package knative

import (
	"context"

	"github.com/sirupsen/logrus"
)

// Functions represent a set of Knative services and a root service used to
// deploy functions to a cluster
type Functions struct {
	config   Config
	cluster  Cluster
	service  Service
	manifest string
}

func (fn *Functions) DeployDryRun() (string, error) {
	logrus.Info("running functions dry-run deployment")

	fn.cluster.SetDryRun()

	return fn.cluster.DeployFunctions(fn.service, fn.manifest)
}

func (fn *Functions) Deploy(ctx context.Context) (string, error) {
	logrus.Info("running functions deployment")

	fn.cluster.SetNoDryRun()

	if fn.config.Registry().HasCredentials() {
		fn.cluster.DeployRegistryCredentials()
	}

	return fn.cluster.DeployFunctions(fn.service, fn.manifest)
}
