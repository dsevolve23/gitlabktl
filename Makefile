VERSION ?= $(shell git describe --tags)

GO_LDFLAGS := -extldflags "-static"
GO_LDFLAGS += -X main.version=$(VERSION)
GO_LDFLAGS += -w -s

.PHONY: test
test:
	go test ./...

.PHONY: build
build: clean
	GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags '$(GO_LDFLAGS)' -o gitlabktl

.PHONY: clean
clean:
	rm -f gitlabktl

.PHONY: image
image: build
	docker build -t registry.gitlab.com/gitlab-org/gitlabktl:dev .
