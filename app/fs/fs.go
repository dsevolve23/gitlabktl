package fs

import (
	"os"
	"path/filepath"

	"github.com/spf13/afero"
)

var afs afero.Fs

func init() {
	SetOsFs()
}

func SetOsFs() {
	afs = afero.NewOsFs()
}

func SetMemFs() {
	afs = afero.NewMemMapFs()
}

func GetFs() afero.Fs {
	return afs
}

func ReadFile(filename string) ([]byte, error) {
	return afero.ReadFile(afs, filename)
}

func WriteFile(filename string, data []byte, perm os.FileMode) error {
	return afero.WriteFile(afs, filename, data, perm)
}

func Exists(path string) (bool, error) {
	return afero.Exists(afs, path)
}

func Create(name string) (afero.File, error) {
	return afs.Create(name)
}

func MkdirAllPath(path string) error {
	directory := filepath.Dir(path)

	return afs.MkdirAll(directory, 0755)
}

func WithTestFs(block func()) {
	SetMemFs()
	defer SetOsFs()

	block()
}

func WithTestFsValue(block func() interface{}) interface{} {
	SetMemFs()
	defer SetOsFs()

	return block()
}
