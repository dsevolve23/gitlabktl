package function

import (
	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/builder"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

var newBuilder = func(action *BuildAction) builder.Builder {
	details := action.toRuntimeDetails()
	runtime := runtime.New(details)

	return builder.NewFromRuntime(runtime)
}

type BuildAction struct {
	Name      string `short:"n" long:"name" env:"GITLAB_FUNCTION_NAME" description:"Function name"`
	File      string `short:"f" long:"file" env:"GITLAB_FUNCTION_FILE" description:"Function file"`
	Handler   string `short:"c" long:"handler" env:"GITLAB_FUNCTION_HANDLER" description:"Function handler"`
	Runtime   string `short:"r" long:"runtime" env:"GITLAB_FUNCTION_RUNTIME" description:"Function runtime"`
	Directory string `short:"d" long:"directory" env:"GITLAB_FUNCTION_DIRECTORY" description:"Directory with code"`
	Image     string `short:"i" long:"image" env:"GITLAB_FUNCTION_IMAGE" description:"Resulting image name"`
	DryRun    bool   `short:"t" long:"dry-run" env:"GITLAB_FUNCTION_DRYRUN" description:"Dry run only"`
}

func (action *BuildAction) Execute(context *commands.Context) error {
	executor := newBuilder(action)

	if action.DryRun {
		_, err := executor.BuildDryRun()

		return err
	}

	return executor.Build(context.Ctx)
}

func (action *BuildAction) toRuntimeDetails() runtime.Details {
	return runtime.Details{
		FunctionName:    action.Name,
		FunctionFile:    action.File,
		FunctionHandler: action.Handler,
		CodeDirectory:   action.Directory,
		RuntimeAddress:  action.Runtime,
		ResultingImage:  action.Image,
	}
}

func newBuildCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "function/build",
			Config: commands.Config{
				Name:        "build",
				Aliases:     []string{},
				Usage:       "Build your serverless function",
				Description: "This commands builds an image with a function and a runtime",
			},
		},
		Handler: new(BuildAction),
	}
}

func RegisterBuildCommand(register *commands.Register) {
	register.RegisterCommand(newBuildCommand())
}
