package application

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestActionSourceDockerfile(t *testing.T) {
	action := buildAction{Dockerfile: "Dockerfile.app"}
	assert.Equal(t, "Dockerfile.app", action.SourceDockerfile())

	action = buildAction{}
	assert.Equal(t, "Dockerfile", action.SourceDockerfile())
}

func TestActionSourceContext(t *testing.T) {
	pwd, err := os.Getwd()
	require.NoError(t, err)

	action := buildAction{Directory: "my/directory"}
	assert.Equal(t, path.Join(pwd, "my/directory"), action.SourceContext())

	action = buildAction{Directory: "/var/my/directory"}
	assert.Equal(t, "/var/my/directory", action.SourceContext())

	action = buildAction{}
	assert.Equal(t, pwd, action.SourceContext())
}

func TestActionBuilderDryRun(t *testing.T) {
	action := buildAction{
		Dockerfile: "Dockerfile",
		Directory:  "build/",
		Image:      "my.registry/my/project",
		Tag:        "0.3.0",
		Aliases:    []string{"latest"},
		DryRun:     true,
	}

	builder := action.Builder()

	summary, err := builder.BuildDryRun()
	require.NoError(t, err)

	assert.Contains(t, summary, "/kaniko/executor")
	assert.Contains(t, summary, "--destination my.registry/my/project:0.3.0")
	assert.Contains(t, summary, "--destination my.registry/my/project:latest")
}
