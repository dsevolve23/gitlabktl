package main

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/gitlabktl/app"
)

var version = "unknown"

func main() {
	fmt.Println("Welcome to gitlabktl tool")

	app := app.NewApp(version)
	app.RegisterCommands()
	err := app.Run(os.Args)

	if err != nil {
		logrus.Fatal(err)
	}
}
