package knative

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestFunctionsDeploy(t *testing.T) {
	service := Service{
		Name:      "my-service",
		Namespace: "my-namespace",
		Image:     "registry.gitlab.com/my/source",
	}

	WithMockCluster(func(cluster *MockCluster) {
		config := new(MockConfig)
		config.On("Registry").
			Return(Registry{Username: "user", Password: "password"}).
			Once()
		defer config.AssertExpectations(t)

		cluster.On("SetNoDryRun").Once()
		cluster.On("DeployRegistryCredentials").Return(nil).Once()
		cluster.On("DeployFunctions", service, mock.Anything).
			Return("deployed", nil).Once()
		defer cluster.AssertExpectations(t)

		app := &Functions{service: service, cluster: cluster, config: config}

		summary, err := app.Deploy(context.Background())
		assert.NoError(t, err)

		assert.Equal(t, "deployed", summary)
	})
}

func TestFunctionsDeployDryRun(t *testing.T) {
	service := Service{
		Name:      "my-service",
		Namespace: "my-namespace",
		Image:     "registry.gitlab.com/my/source",
	}

	WithMockCluster(func(cluster *MockCluster) {
		cluster.On("SetDryRun").Once()
		cluster.On("DeployFunctions", service, mock.Anything).
			Return("deployed", nil).Once()
		defer cluster.AssertExpectations(t)

		app := &Functions{service: service, cluster: cluster}

		summary, err := app.DeployDryRun()
		assert.NoError(t, err)

		assert.Equal(t, "deployed", summary)
	})
}
