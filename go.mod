module gitlab.com/gitlab-org/gitlabktl

go 1.12

require (
	git.apache.org/thrift.git v0.0.0-20180902110319-2566ecd5d999 // indirect
	github.com/gobuffalo/packr/v2 v2.0.0-rc.14 // indirect
	github.com/google/go-containerregistry v0.0.0-20190322231611-80cc55a02b42 // indirect
	github.com/gosimple/slug v1.4.2
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/markbates/deplist v1.0.5 // indirect
	github.com/openzipkin/zipkin-go v0.1.6 // indirect
	github.com/petar/GoLLRB v0.0.0-20130427215148-53be0d36a84c // indirect
	github.com/pkg/errors v0.8.1
	github.com/pkg/profile v1.2.1 // indirect
	github.com/prometheus/client_golang v0.9.3-0.20190127221311-3c4408c8b829 // indirect
	github.com/rainycape/unidecode v0.0.0-20150907023854-cb7f23ec59be // indirect
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/afero v1.2.2
	github.com/stretchr/testify v1.3.0
	github.com/triggermesh/tm v0.0.12-0.20190606132652-fd9e7a4923a0
	github.com/urfave/cli v1.20.0
	gitlab.com/ayufan/golang-cli-helpers v0.0.0-20171103152739-a7cf72d604cd
	golang.org/x/build v0.0.0-20190111050920-041ab4dc3f9d // indirect
	golang.org/x/tools v0.0.0-20190319232107-3f1ed9edd1b4 // indirect
	gopkg.in/src-d/go-billy.v4 v4.3.0
	gopkg.in/src-d/go-git.v4 v4.10.0
	gopkg.in/yaml.v2 v2.2.2
)
