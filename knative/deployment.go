package knative

import (
	"github.com/pkg/errors"

	"gitlab.com/gitlab-org/gitlabktl/serverless"
)

type Deployer serverless.Deployer

// We currently have both DeployFunctions() and DeployApplications() because
// `tm` tool deploys them differently.

func NewFunctionsDeployer(service Service, manifest string, config Config) (Deployer, error) {
	cluster, err := NewCluster(config)
	if err != nil {
		return nil, errors.Wrap(err, "could not define a deployment cluster")
	}

	deployer := &Functions{
		service:  service,
		manifest: manifest,
		cluster:  cluster,
		config:   config}

	return deployer, nil
}

func NewApplicationDeployer(service Service, config Config) (Deployer, error) {
	cluster, err := NewCluster(config)
	if err != nil {
		return nil, errors.Wrap(err, "could not define a deployment cluster")
	}

	deployer := &Application{service: service, cluster: cluster, config: config}

	return deployer, nil
}
