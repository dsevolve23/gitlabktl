package runtime

import (
	"path"

	"github.com/pkg/errors"
	"gopkg.in/src-d/go-billy.v4/memfs"
	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/storage/memory"

	"gitlab.com/gitlab-org/gitlabktl/app/fs"
)

type Client interface {
	WriteFiles(directory string) error
	ReadFile(path string) (string, error)
}

type GoGit struct {
	Location
	*git.Repository
}

func NewClient(location Location) (Client, error) {
	repository, err := git.Clone(memory.NewStorage(), memfs.New(), &git.CloneOptions{
		URL: location.Address,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to clone repository")
	}

	worktree, err := repository.Worktree()
	if err != nil {
		return nil, errors.Wrap(err, "could not fetch the repository worktree")
	}

	err = worktree.Checkout(&git.CheckoutOptions{
		Branch: plumbing.NewBranchReferenceName(location.Reference),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not checkout the runtime version")
	}

	return &GoGit{Location: location, Repository: repository}, nil
}

func (client *GoGit) WriteFiles(directory string) error {
	commit, err := client.headCommit()
	if err != nil {
		return errors.Wrap(err, "could not find HEAD commit")
	}

	iterator, err := commit.Files()
	if err != nil {
		return errors.Wrap(err, "could not find requested file")
	}

	return iterator.ForEach(func(file *object.File) error {
		path := path.Join(directory, file.Name)

		err := fs.MkdirAllPath(path)
		if err != nil {
			return err
		}

		contents, err := file.Contents()
		if err != nil {
			return err
		}

		return fs.WriteFile(path, []byte(contents), 0644)
	})
}

func (client *GoGit) ReadFile(path string) (string, error) {
	commit, err := client.headCommit()
	if err != nil {
		return "", errors.Wrap(err, "could not find HEAD commit")
	}

	file, err := commit.File(path)
	if err != nil {
		return "", errors.Wrap(err, "could not find requested file")
	}

	contents, err := file.Contents()
	if err != nil {
		return "", errors.Wrap(err, "could not read requested file contents")
	}

	return contents, nil
}

func (client *GoGit) headCommit() (*object.Commit, error) {
	ref, err := client.Head()
	if err != nil {
		return new(object.Commit), errors.Wrap(err, "could not fetch repository HEAD")
	}

	commit, err := client.CommitObject(ref.Hash())
	if err != nil {
		return new(object.Commit), errors.Wrap(err, "could not find HEAD commit")
	}

	return commit, nil
}
