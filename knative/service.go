package knative

import (
	tmservice "github.com/triggermesh/tm/pkg/resources/service"
)

// Service describes a basic Knative service that can be deployed to a Knative
// cluster and served by Knative Serving
type Service struct {
	Name      string
	Image     string
	Namespace string
	Secrets   []string
	Envs      []string
}

type tmService struct {
	tmservice.Service
}

func newTriggermeshService(service Service, cluster Cluster) (*tmService, error) {
	tmservice := &tmService{
		Service: tmservice.Service{
			Name:       service.Name,
			Namespace:  service.Namespace,
			Source:     service.Image,
			EnvSecrets: service.Secrets,
			Env:        service.Envs,
		},
	}

	if len(tmservice.Namespace) == 0 {
		tmservice.Namespace = cluster.Namespace()
	}

	return tmservice, nil
}
