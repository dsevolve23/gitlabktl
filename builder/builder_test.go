package builder

import (
	"testing"

	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/runtime"
)

func TestBuildNewFromRuntime(t *testing.T) {
	runtime := new(runtime.MockRuntime)

	runtime.On("Directory").Return("my/func").Once()
	runtime.On("Image").Return("my/image:latest").Once()
	runtime.On("DockerfilePath").Return("my/Dockerfile.test").Once()

	config := env.Stubs{
		"CI_REGISTRY":          "registry.test",
		"CI_REGISTRY_USER":     "my-user",
		"CI_REGISTRY_PASSWORD": "my-password",
	}

	env.WithStubbedEnv(config, func() { NewFromRuntime(runtime) })

	runtime.AssertExpectations(t)
}
