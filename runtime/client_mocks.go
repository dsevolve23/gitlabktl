package runtime

import (
	"github.com/stretchr/testify/mock"
)

type MockClient struct {
	mock.Mock
}

func (client *MockClient) ReadFile(path string) (string, error) {
	args := client.Called(path)

	return args.String(0), args.Error(1)
}

func (client *MockClient) WriteFiles(directory string) error {
	args := client.Called(directory)

	return args.Error(0)
}
