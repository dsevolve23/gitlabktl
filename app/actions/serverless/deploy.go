package serverless

import (
	"github.com/pkg/errors"

	"gitlab.com/gitlab-org/gitlabktl/app/commands"
	"gitlab.com/gitlab-org/gitlabktl/app/env"
	"gitlab.com/gitlab-org/gitlabktl/config"
	"gitlab.com/gitlab-org/gitlabktl/knative"
	"gitlab.com/gitlab-org/gitlabktl/registry"
)

// DeployAction struct is going to be populated with CLI invocation arguments.
type DeployAction struct {
	DryRun         bool   `short:"t" long:"dry-run" env:"GITLAB_DRYRUN" description:"Dry run only"`
	ServerlessFile string `short:"f" long:"file" env:"GITLAB_SERVERLESS_FILE" description:"Path to serverless manifest file"`
	KubeConfigFile string `short:"c" long:"kubeconfig" env:"GITLAB_KUBECONFIG_FILE" description:"Path to kubeconfig"`
}

func (action *DeployAction) Execute(context *commands.Context) (err error) {
	service := knative.Service{Namespace: action.Namespace()}

	deployer, err := knative.NewFunctionsDeployer(service, action.Manifest(), action)
	if err != nil {
		return errors.Wrap(err, "could not create functions deployer")
	}

	if action.DryRun {
		_, err = deployer.DeployDryRun()
	} else {
		_, err = deployer.Deploy(context.Ctx)
	}

	return err
}

func (action *DeployAction) Registry() knative.Registry {
	source := registry.NewWithPullAccess()

	return knative.Registry{
		Host:       source.Host,
		Username:   source.Username,
		Password:   source.Password,
		Repository: registry.DefaultRepository(),
	}
}

func (action *DeployAction) Namespace() string {
	return env.Getenv("KUBE_NAMESPACE")
}

func (action *DeployAction) Manifest() string {
	if len(action.ServerlessFile) > 0 {
		return action.ServerlessFile
	}

	return config.ServerlessFile
}

func (action *DeployAction) KubeConfig() string {
	return config.NewKubeConfigPath(action.KubeConfigFile)
}

func newDeployCommand() commands.Command {
	return commands.Command{
		Registrable: commands.Registrable{
			Path: "serverless/deploy",
			Config: commands.Config{
				Name:        "deploy",
				Aliases:     []string{},
				Usage:       "Deploy your serverless services",
				Description: "This commands deploys your images to Knative cluster",
			},
		},
		Handler: new(DeployAction),
	}
}

func RegisterDeployCommand(register *commands.Register) {
	register.RegisterCommand(newDeployCommand())
}
